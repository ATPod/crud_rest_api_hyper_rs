﻿CREATE TABLE message
(
  uuid character varying NOT NULL,
  user_name character varying NOT NULL,
  user_email character varying NOT NULL,
  user_message character varying NOT NULL,
  CONSTRAINT message_pkey PRIMARY KEY (uuid)
)