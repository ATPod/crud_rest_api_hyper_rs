use dotenv::dotenv;
use std::env;

use postgres::{Connection, TlsMode};
//TODO add connection pool (e.g. r2d2-postgres)
pub fn get_connection() -> Connection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    Connection::connect(database_url, TlsMode::None).unwrap()
}