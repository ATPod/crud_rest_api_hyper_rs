extern crate futures;
extern crate hyper;
extern crate pretty_env_logger;
extern crate url;

#[macro_use]
extern crate serde_derive;
extern crate serde;
#[macro_use]
extern crate serde_json;

extern crate postgres;
extern crate dotenv;

extern crate uuid;

use futures::{future, Future, Stream};

use hyper::{Body, Method, Request, Response, Server, StatusCode, header};
use hyper::service::service_fn;

pub mod model;
use model::Message;

use uuid::Uuid;

pub mod db_connection;
use db_connection::get_connection;

static SEPARATOR: &str = r"=";

pub fn params_validate(query : &str) -> bool {
    let v: Vec<&str> = query.split(SEPARATOR).collect();
    v.len() == 2
}
// Using service_fn, we can turn this function into a `Service`.
fn message_api(req: Request<Body>) -> Box<Future<Item=Response<Body>, Error=hyper::Error> + Send> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/api/v1") => {
            let res = Response::builder()
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(r#"{
                "version":"1.0"
                }"#))
                .unwrap();
            Box::new(future::ok(res))
        },
        (&Method::POST, "/api/v1/message") => {
            Box::new(req.into_body().concat2().map(|b|{
                let message: Message = serde_json::from_slice(&b).unwrap();
                println!("INSERT: {:?}", message);
                let connection = get_connection();
                let uuid = Uuid::new_v4();
                connection.execute("INSERT INTO message (uuid, \
                    user_name, user_email, user_message) VALUES ($1, $2, $3, $4)",
                             &[&uuid.hyphenated().to_string(),
                                 &message.user_name, &message.user_email, &message.user_message]).unwrap();
                let status = json!({
                    "uuid": &uuid.hyphenated().to_string()
                });
                Response::new(status.to_string().into())
            }))
        },
        (&Method::GET, "/api/v1/message") => {
            // request URL looks like this: http://localhost:1337/api/v1/message?message_id=4510e80d-13d3-4ab6-a3f2-7ef010244fd6
            let res = match req.uri().query() {
                Some(inner) => {
                    println!("SELECT: {:?}", inner);
                    let mut messages = Vec::new();
                    if params_validate(inner) {
                        let params: Vec<&str> = inner.split(SEPARATOR).collect();
                        let connection = get_connection();
                        //TODO add rows processing
                        for row in
                            &connection.query("SELECT uuid, user_name, user_email, user_message FROM message WHERE uuid = $1",
                                              &[&params[1]]).unwrap() {
                            let uuid: String = row.get(0);
                            let user_name: String = row.get(1);
                            let user_email: String = row.get(2);
                            let user_message: String = row.get(3);
                            println!("uuid: {}, user_name: {}, user_email: {}, user_message: {}",
                                     uuid, user_name, user_email, user_message);
                            let status = json!({
                                "uuid": &uuid.to_string(),
                                "user_name": &user_name.to_string(),
                                "user_email": &user_email.to_string(),
                                "user_message": &user_message.to_string()
                            });
                            messages.push(status);
                        }
                        println!("messages {:?}", messages[0].to_string());
                    } else {
                        Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::from(r#"{
                                "error":"Parameters are not valid!"
                                }"#)).unwrap();
                    }
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(Body::from(messages[0].to_string()))
                        .unwrap()
                },
                None => {
                    Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(Body::from(r#"{
                    "version":"Parameters are not valid!"
                    }"#))
                    .unwrap()
                }
            };
            Box::new(future::ok(res))
        },(&Method::DELETE, "/api/v1/message") => {
            let res = match req.uri().query() {
                Some(inner) => {
                    println!("DELETE: {:?}", inner);
                    let mut status:serde_json::Value = serde_json::Value::Null;
                    if params_validate(inner) {
                        let params: Vec<&str> = inner.split(SEPARATOR).collect();
                        &get_connection().execute("DELETE FROM message WHERE uuid = $1", &[&params[1]]).unwrap();
                        status = json!({
                            "uuid": &params[1].to_string()
                         });
                    } else {
                        Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::from(r#"{
                                "error":"Parameters are not valid!"
                                }"#)).unwrap();
                    }
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(Body::from(status.to_string()))
                        .unwrap()
                },
                None => {
                    Response::builder()
                        .status(StatusCode::NOT_FOUND)
                        .body(Body::from(r#"{
                    "version":"Parameters are not valid!"
                    }"#))
                        .unwrap()
                }
            };
            Box::new(future::ok(res))
        },
        _ => {
            Box::new(future::ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Body::empty())
                .unwrap()))
        }
    }

}



fn main() {
    pretty_env_logger::init();

    let addr = ([127, 0, 0, 1], 1337).into();

    let server = Server::bind(&addr)
        .serve(|| service_fn(message_api))
        .map_err(|e| eprintln!("server error: {}", e));

    hyper::rt::run(server);
}