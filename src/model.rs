#[derive(Deserialize, Debug)]
pub struct Message {
    pub user_name: String,
    pub user_email: String,
    pub user_message: String,
}
